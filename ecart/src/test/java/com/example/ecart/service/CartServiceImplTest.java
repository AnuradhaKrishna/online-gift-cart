package com.example.ecart.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.ecart.dto.CartRequestDto;
import com.example.ecart.dto.CartUpdateDto;
import com.example.ecart.dto.ResponseDto;
import com.example.ecart.dto.UserDto;
import com.example.ecart.entity.Cart;
import com.example.ecart.entity.Customer;
import com.example.ecart.entity.Product;
import com.example.ecart.entity.User;
import com.example.ecart.exception.CartException;
import com.example.ecart.repository.CartRepository;
import com.example.ecart.repository.CustomerRepository;
import com.example.ecart.repository.ProductRepository;
import com.example.ecart.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CartServiceImplTest {

	@InjectMocks
	CartSeviceImpl cartServiceImpl;

	@Mock
	CartRepository cartRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	ProductRepository productRepository;

	@Mock
	UserRepository userRepository;

	@Mock
	EmailSender emailSender;

	@Test
	public void cartServiceTest() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Corporate");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(10);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Corporate");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(10);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(100);
		product.setProductId(1);

		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);

		Mockito.when(cartRepository.findByCartId(Mockito.anyInt())).thenReturn(cart);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cart);

		Mockito.when(userRepository.findByUserId(Mockito.anyInt())).thenReturn(user);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

		ResponseDto result = cartServiceImpl.updateCart(cartUpdateDto);
		assertNotNull(result);

	}

	@Test
	public void cartServiceTests() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Corporate");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(10);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(10);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(100);
		product.setProductId(1);

		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);

		Mockito.when(cartRepository.findByCartId(Mockito.anyInt())).thenReturn(cart);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cart);

		Mockito.when(userRepository.findByUserId(Mockito.anyInt())).thenReturn(user);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

		ResponseDto result = cartServiceImpl.updateCart(cartUpdateDto);
		assertNotNull(result);

	}

	@Test
	public void cartServiceTest1() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Personal");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(3);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Corporate");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(5);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(100);
		product.setProductId(1);

		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);

		Mockito.when(cartRepository.findByCartId(Mockito.anyInt())).thenReturn(cart);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cart);

		Mockito.when(userRepository.findByUserId(Mockito.anyInt())).thenReturn(user);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

		ResponseDto result = cartServiceImpl.updateCart(cartUpdateDto);
		assertNotNull(result);
	}

	@Test(expected = CartException.class)
	public void cartServiceTest3() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Corporate");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(3);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(3);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(100);
		product.setProductId(1);

		cartServiceImpl.updateCart(cartUpdateDto);

	}

	@Test(expected = CartException.class)
	public void cartServiceTest4() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Personal");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(10);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Corporate");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(10);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(100);
		product.setProductId(1);

		cartServiceImpl.updateCart(cartUpdateDto);

	}

	@Test(expected = CartException.class)
	public void cartServiceTest5() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Corporate");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(5);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(3);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(1);
		product.setProductId(1);

		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);

		Mockito.when(cartRepository.findByCartId(Mockito.anyInt())).thenReturn(cart);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cart);

		Mockito.when(userRepository.findByUserId(Mockito.anyInt())).thenReturn(user);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

		cartServiceImpl.updateCart(cartUpdateDto);

	}

	@Test(expected = CartException.class)
	public void cartServiceTest6() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Personal");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(3);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Corporate");
		cart.setCustomerId(1);
		cart.setProductId(1);
		cart.setPrice(5000d);
		cart.setQuantity(5);
		cart.setStatus("pending");

		User user = new User();
		user.setCartId(1);
		user.setEmailId("haran123@gmail.com");
		user.setUserId(1);
		user.setUserName("Haran");

		Product product = new Product();
		product.setPrice(500d);
		product.setProductName("Football");
		product.setQuantity(1);
		product.setProductId(1);

		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);

		Mockito.when(cartRepository.findByCartId(Mockito.anyInt())).thenReturn(cart);
		Mockito.when(cartRepository.save(Mockito.any())).thenReturn(cart);

		Mockito.when(userRepository.findByUserId(Mockito.anyInt())).thenReturn(user);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

		cartServiceImpl.updateCart(cartUpdateDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest1() throws CartException {
		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personal");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(12);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Baiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setPrice(12000d);
		cart.setProductId(1);
		cart.setQuantity(12);
		cart.setStatus("Pending");

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");

		Product product = new Product();
		product.setPrice(12000d);
		product.setProductName("Cup");
		product.setQuantity(12);

		Mockito.when(customerRepository.findByCustomerId(null)).thenReturn(Optional.of(customer));
		// Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		// Mockito.when(userRepository.save(Mockito.any())).thenReturn(Mockito.any());

		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest3() throws CartException {

		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personal");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(12);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Baiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");
		Product product = new Product();
		product.setPrice(12000d);
		product.setProductName("Cup");
		product.setQuantity(2);

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest4() throws CartException {

		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Perso");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(12);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Baiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");
		Product product = new Product();
		product.setPrice(12000d);
		product.setProductName("Cup");
		product.setQuantity(22);

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest5() throws CartException {

		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personal");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(4);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Baiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");
		Product product = new Product();
		product.setPrice(12000d);
		product.setProductName("Cup");
		product.setQuantity(22);

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest6() throws CartException {

		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Corporate");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(1);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Baiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");
		Product product = new Product();
		product.setPrice(12000d);
		product.setProductName("Cup");
		product.setQuantity(22);

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test(expected = CartException.class)
	public void addProductsTest() throws CartException {
		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personal");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(1);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Hari");

		UserDto userDto1 = new UserDto();
		userDto1.setEmailId("abc@gmail.com");
		userDto1.setMessage("Gift");
		userDto1.setUserName("Hari");

		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		user.add(userDto1);
		cartRequestDto.setUserDto(user);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setPrice(200d);
		cart.setProductId(1);
		cart.setQuantity(2);
		cart.setStatus("Pending");

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");

		Product product = new Product();
		product.setPrice(100d);
		product.setProductName("Cup");
		product.setQuantity(10);
		product.setProductId(1);

		User users = new User();
		users.setCartId(1);
		users.setEmailId("abc@gmail.com");
		users.setMessage("Gift");
		users.setUserId(1);
		users.setUserName("Hari");

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(Mockito.any());

		cartServiceImpl.addProducts(cartRequestDto);

	}

	@Test
	public void addProductsTestPositive() throws CartException {
		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personal");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(2);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("Hari");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(200);

		Cart cart = new Cart();
		cart.setCartId(1);
		cart.setCategory("Personal");
		cart.setCustomerId(1);
		cart.setPrice(200d);
		cart.setProductId(1);
		cart.setQuantity(2);
		cart.setStatus("Pending");

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setEmailId("abc@gmail.com");

		Product product = new Product();
		product.setPrice(100d);
		product.setProductName("Cup");
		product.setQuantity(10);
		product.setProductId(1);

		User users = new User();
		users.setCartId(1);
		users.setEmailId("abc@gmail.com");
		users.setMessage("Gift");
		users.setUserId(1);
		users.setUserName("Hari");

		Mockito.when(customerRepository.findByCustomerId(1)).thenReturn(Optional.of(customer));
		Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(Mockito.any());

		cartServiceImpl.addProducts(cartRequestDto);

	}

}
