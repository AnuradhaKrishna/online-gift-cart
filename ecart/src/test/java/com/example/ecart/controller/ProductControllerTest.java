package com.example.ecart.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.ecart.entity.Product;
import com.example.ecart.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	@InjectMocks
	ProductController productController;

	@Mock
	ProductService productService;

	@Test
	public void getListOfProductsTest() {
		List<Product> product = new ArrayList<>();
		Mockito.when(productService.getListOfProducts()).thenReturn(product);

		ResponseEntity<List<Product>> result = productController.getListOfProducts();
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}
}
