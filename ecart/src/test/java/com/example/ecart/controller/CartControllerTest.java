package com.example.ecart.controller;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.example.ecart.dto.CartRequestDto;
import com.example.ecart.dto.CartUpdateDto;
import com.example.ecart.dto.ResponseDto;
import com.example.ecart.dto.UserDto;
import com.example.ecart.entity.Cart;
import com.example.ecart.exception.CartException;
import com.example.ecart.service.CartService;

@RunWith(MockitoJUnitRunner.class)
public class CartControllerTest {

	@InjectMocks
	CartController cartController;

	@Mock
	CartService cartService;

	@Test
	public void addProductTest() throws CartException {
		CartRequestDto cartRequestDto = new CartRequestDto();
		cartRequestDto.setCategory("Personel");
		cartRequestDto.setCustomerId(1);
		cartRequestDto.setProductId(1);
		cartRequestDto.setQuantity(12);
		UserDto userDto = new UserDto();
		userDto.setEmailId("abc@gmail.com");
		userDto.setMessage("Gift");
		userDto.setUserName("BAiu");
		List<UserDto> user = new ArrayList<>();
		user.add(userDto);
		cartRequestDto.setUserDto(user);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(200);

		Mockito.when(cartService.addProducts(Mockito.any())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = cartController.addProduct(Mockito.any());
		assertNotNull(result);

	}

	@Test
	public void confirmOrder() throws CartException {
		Cart cart = new Cart();
		cart.setCartId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(200);
		Mockito.when(cartService.confirmOrder(Mockito.anyInt())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = cartController.confirmOrder(1);
		assertNotNull(result);
	}

	@Test
	public void cartController() throws CartException {
		CartUpdateDto cartUpdateDto = new CartUpdateDto();
		cartUpdateDto.setCartId(1);
		cartUpdateDto.setCategory("Corporate");
		cartUpdateDto.setEmailId("haran123@gmail.com");
		cartUpdateDto.setUserId(1);
		cartUpdateDto.setUserName("Haran");
		cartUpdateDto.setQuantity(10);
		cartUpdateDto.setProductId(1);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Mockito.when(cartService.updateCart(cartUpdateDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = cartController.update(cartUpdateDto);
		assertNotNull(result);
	}

}
