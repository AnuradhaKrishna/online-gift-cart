package com.example.ecart.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ecart.dto.CartRequestDto;
import com.example.ecart.dto.CartUpdateDto;
import com.example.ecart.dto.ResponseDto;
import com.example.ecart.exception.CartException;
import com.example.ecart.service.CartService;

@RestController
@RequestMapping("/products")
public class CartController {
	@Autowired
	CartService cartService;

	@PutMapping("")
	public ResponseEntity<ResponseDto> confirmOrder(@RequestParam(value = "cartId") Integer cartId)
			throws CartException {
		return new ResponseEntity<>(cartService.confirmOrder(cartId), HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<ResponseDto> addProduct(@RequestBody CartRequestDto cartRequestDto) throws CartException {
		ResponseDto responseDto = cartService.addProducts(cartRequestDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PutMapping("/cart")
	public ResponseEntity<ResponseDto> update(@Valid @RequestBody CartUpdateDto cartUpdateDto) throws CartException {
		ResponseDto responseDto = cartService.updateCart(cartUpdateDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

}
