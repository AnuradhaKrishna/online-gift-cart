package com.example.ecart.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ecart.entity.Product;
import com.example.ecart.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Product> getListOfProducts() {
		List<Product> products = productRepository.findAll();
		List<Product> productList = new ArrayList<>();
		products.forEach(product1 -> {
			Product product = new Product();
			product.setPrice(product1.getPrice());
			product.setProductId(product1.getProductId());
			product.setProductName(product1.getProductName());
			product.setQuantity(product1.getQuantity());
			productList.add(product1);

		});

		return productList;
	}

}
