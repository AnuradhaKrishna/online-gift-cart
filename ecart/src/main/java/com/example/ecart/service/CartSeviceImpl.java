package com.example.ecart.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ecart.constants.ApplicationConstants;
import com.example.ecart.dto.CartRequestDto;
import com.example.ecart.dto.CartUpdateDto;
import com.example.ecart.dto.ResponseDto;
import com.example.ecart.dto.UserDto;
import com.example.ecart.entity.Cart;
import com.example.ecart.entity.Customer;
import com.example.ecart.entity.Product;
import com.example.ecart.entity.PurchasedHistory;
import com.example.ecart.entity.User;
import com.example.ecart.exception.CartException;
import com.example.ecart.repository.CartRepository;
import com.example.ecart.repository.CustomerRepository;
import com.example.ecart.repository.ProductRepository;
import com.example.ecart.repository.PurchasedHistoryRepository;
import com.example.ecart.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CartSeviceImpl implements CartService {

	@Autowired
	CartRepository cartRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	PurchasedHistoryRepository purchasedHistoryRepository;

	@Autowired
	EmailSender emailSender;

	@Override
	public ResponseDto confirmOrder(Integer cartId) throws CartException {
		ResponseDto responseDto = new ResponseDto();
		PurchasedHistory purchasedHistory = new PurchasedHistory();
		Cart cart = cartRepository.findByCartId(cartId);
		cart.setStatus(ApplicationConstants.STATUS_CONFIRM);
		Optional<Customer> customer = customerRepository.findByCustomerId(cart.getCustomerId());
		Optional<List<User>> users = userRepository.findByCartId(cartId);
		if (!users.isPresent()) {
			throw new CartException(ApplicationConstants.USER_NOT_PRESENT);
		}
		users.get().forEach(user -> {
			// User userList
			String bodyMessage = "The order is placed for " + user.getUserName() + " with a " + user.getMessage()
					+ " from ur friend" + customer.get().getCustomerName();
			emailSender.sendOtp(user.getEmailId(), "confirmation Of order", bodyMessage);

		});
		purchasedHistory.setCartId(cartId);
		purchasedHistory.setPurchasedDate(LocalDate.now());
		purchasedHistoryRepository.save(purchasedHistory);
		responseDto.setMessage(ApplicationConstants.ORDER_PLACED);
		responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		return responseDto;
	}

	@Override
	public ResponseDto addProducts(CartRequestDto cartRequestDto) throws CartException {
		Cart cart = new Cart();
		ResponseDto responseDto = new ResponseDto();
		Optional<Customer> customer = customerRepository.findByCustomerId(cartRequestDto.getCustomerId());
		if (!customer.isPresent()) {
			throw new CartException(ApplicationConstants.CUSTOMER_NOT_REGISTERED);
		}
		Product product = productRepository.findByProductId(cartRequestDto.getProductId());
		if (cartRequestDto.getQuantity() > product.getQuantity()) {
			throw new CartException(ApplicationConstants.QUANTITY_LESS);
		} else {
			Integer quantity = product.getQuantity() - cartRequestDto.getQuantity();
			product.setQuantity(quantity);
		}
		if (!(cartRequestDto.getCategory().equalsIgnoreCase(ApplicationConstants.CORPORATE)
				|| cartRequestDto.getCategory().equalsIgnoreCase(ApplicationConstants.PERSONAL))) {
			throw new CartException(ApplicationConstants.CATEGORY_ERROR);
		}
		if (cartRequestDto.getCategory().equalsIgnoreCase(ApplicationConstants.PERSONAL))

		{
			if ((cartRequestDto.getQuantity()) > 3) {
				throw new CartException(ApplicationConstants.FOR_PERSONAL_MAX_3);
			}
		}
		if (cartRequestDto.getCategory().equalsIgnoreCase(ApplicationConstants.CORPORATE)) {
			if ((cartRequestDto.getQuantity()) < 5) {
				throw new CartException(ApplicationConstants.FOR_CORPORATE_MIN_5);
			}
		}
		cart.setCategory(cartRequestDto.getCategory());
		cart.setCustomerId(cartRequestDto.getCustomerId());
		cart.setProductId(cartRequestDto.getProductId());
		cart.setQuantity(cartRequestDto.getQuantity());
		double price = product.getPrice() * cartRequestDto.getQuantity();
		cart.setPrice(price);
		BeanUtils.copyProperties(cartRequestDto, cart);
		cart.setStatus(ApplicationConstants.PENDING);
		cartRepository.save(cart);
		List<UserDto> userDtos = cartRequestDto.getUserDto();
		userDtos.forEach(list -> {
			User user = new User();
			user.setEmailId(list.getEmailId());
			user.setMessage(list.getMessage());
			user.setUserName(list.getUserName());
			user.setCartId(cart.getCartId());
			userRepository.save(user);
		});
		int size = userDtos.size();
		log.info("Size" + size);
		if (size > cartRequestDto.getQuantity()) {
			throw new CartException(ApplicationConstants.USER_DTO_SIZE_ERROR);
		}
		responseDto.setMessage(ApplicationConstants.ADDED);
		responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		return responseDto;
	}

	@Override
	public ResponseDto updateCart(CartUpdateDto cartUpdateDto) throws CartException {
		ResponseDto responseDto = new ResponseDto();
		String category = cartUpdateDto.getCategory();
		
		log.info("CATEGORY" + category);
		// Personel to Corporate
		if (category.equalsIgnoreCase(ApplicationConstants.CORPORATE)) {
			if (cartUpdateDto.getQuantity() > 4) {
				Integer cartQuantity = cartUpdateDto.getQuantity();
				Product productById = productRepository.findByProductId(cartUpdateDto.getProductId());
				Cart cartById = cartRepository.findByCartId(cartUpdateDto.getCartId());
				Integer productQuantity = productById.getQuantity();
				cartById.setProductId(cartUpdateDto.getProductId());
				Double productPrice = productById.getPrice();
				if (productQuantity > cartQuantity) {
					Integer finalQuantity = productQuantity - cartQuantity;
					productById.setQuantity(finalQuantity);
					productRepository.save(productById);
					Double finalPrice = productPrice * cartQuantity;
					cartById.setPrice(finalPrice);
					cartById.setQuantity(cartQuantity);
					cartById.setCategory(ApplicationConstants.CORPORATE);
					cartRepository.save(cartById);
					User users = userRepository.findByUserId(cartUpdateDto.getUserId());
					users.setEmailId(cartUpdateDto.getEmailId());
					users.setUserName(cartUpdateDto.getUserName());
					userRepository.save(users);
					responseDto.setMessage(ApplicationConstants.UPDATED_SUCCESSFULLY);
					responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
				} else {
					throw new CartException(ApplicationConstants.QUANTITY_LESS);
				}
			} else {
				throw new CartException(ApplicationConstants.MAXIMUM_VALUE);
			}
		} // Corporate to Personel
		if (category.equalsIgnoreCase(ApplicationConstants.PERSONAL)) {
			if (cartUpdateDto.getQuantity() < 4) {
				Integer cartQuantity = cartUpdateDto.getQuantity();
				Product productById = productRepository.findByProductId(cartUpdateDto.getProductId());
				Cart cartById = cartRepository.findByCartId(cartUpdateDto.getCartId());
				Integer productQuantity = productById.getQuantity();
				Double productPrice = productById.getPrice();
				cartById.setProductId(cartUpdateDto.getProductId());
				if (productQuantity > cartQuantity) {
					Integer finalQuantity = productQuantity + cartQuantity;
					productById.setQuantity(finalQuantity);
					productRepository.save(productById);
					Double finalPrice = productPrice * cartQuantity;
					cartById.setPrice(finalPrice);
					cartById.setQuantity(cartQuantity);
					cartById.setCategory(ApplicationConstants.PERSONAL);
					cartRepository.save(cartById);
					User users = userRepository.findByUserId(cartUpdateDto.getUserId());
					users.setEmailId(cartUpdateDto.getEmailId());
					users.setUserName(cartUpdateDto.getUserName());
					userRepository.save(users);
					responseDto.setMessage(ApplicationConstants.UPDATED_SUCCESSFULLY);
					responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
				} else {
					throw new CartException(ApplicationConstants.QUANTITY_LESS);
				}
			} else {
				throw new CartException(ApplicationConstants.MINIMUM_VALUE);
			}
		}
		return responseDto;
	}
}