package com.example.ecart.service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EmailSender {

	@Autowired
	private JavaMailSender javaMailSender;

	public String sendOtp(String mailId, String headMessage, String bodyMessage) {

		String returnString = "EMAIL SENT SUCCESSFULLY!!!";
		try {

			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);

			helper.setTo(mailId);
			helper.setSubject(headMessage);
			helper.setText(bodyMessage);
			message.addRecipient(RecipientType.CC, new InternetAddress(
		            "akrathour08@gmail.com"));

			javaMailSender.send(message);

		} catch (Exception e) {
			returnString = "EMAIL FAILED TO SEND!!!";
			log.info(e.getMessage());
		}
		return returnString;

	}

}
