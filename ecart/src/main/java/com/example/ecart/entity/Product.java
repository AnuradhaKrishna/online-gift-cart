package com.example.ecart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Product {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer productId;
	private Double price;
	private Integer quantity;    
	private String productName;   

}
