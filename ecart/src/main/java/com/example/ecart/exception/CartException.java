package com.example.ecart.exception;

public class CartException extends Exception{
	private static final long serialVersionUID = 1L;

	public CartException(String exception) {

		super(exception);
	}

}