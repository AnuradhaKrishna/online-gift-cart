package com.example.ecart.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorMessage {

	private String message;
	private int status;

	public ErrorMessage() {
		super();
	}

}
