package com.example.ecart.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputErrorMessage {

	private String message;
	private int statusCode;
	private List<String> errorMessage;

	public InputErrorMessage(String message, List<String> errorMessage, int statusCode) {
		this.message = message;
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
	}

	public InputErrorMessage() {
		super();
	}
}
