package com.example.ecart.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.ecart.constants.ApplicationConstants;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
			org.springframework.http.HttpHeaders headers, HttpStatus statusCode, WebRequest request) {
		if (ex instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
			List<String> errorList = exception.getBindingResult().getFieldErrors().stream()
					.map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList());
			InputErrorMessage errorDetails = new InputErrorMessage("THIS IS THE CUSTOM EXCEPTION HANDLER MESSAGE",
					errorList, ApplicationConstants.UNSUCCESS_CODE);
			return super.handleExceptionInternal(ex, errorDetails, headers, statusCode, request);
		}
		return super.handleExceptionInternal(ex, body, headers, statusCode, request);
	}

	@ExceptionHandler(CartException.class)
	ResponseEntity<ErrorMessage> cartException(CartException cartException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(cartException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

}
