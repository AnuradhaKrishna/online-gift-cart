package com.example.ecart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

	private String userName;
	private String emailId;
	private String message;

}
