package com.example.ecart.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartRequestDto {

	private Integer productId;
	private Integer customerId;
	private Integer quantity;
	private String category;
	private List<UserDto> userDto;

}
