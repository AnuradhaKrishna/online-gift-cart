package com.example.ecart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartUpdateDto {

	private String category;
	private Integer quantity;
	private Integer productId;
	private Integer cartId;
	private Integer userId;
	private String emailId;
	private String userName;

}
