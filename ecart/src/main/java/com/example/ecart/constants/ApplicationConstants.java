package com.example.ecart.constants;

public class ApplicationConstants {
	private ApplicationConstants() {

	}

	public static final Integer SUCCESS_CODE = 607;
	public static final Integer UNSUCCESS_CODE = 608;
	public static final String PRODUCT_NOT_PRESENT = "PRODUCT IS NOT PRESENT IN THE CART!!!";
	public static final String STATUS_CONFIRM = "CONFIRM";
	public static final String USER_NOT_PRESENT = "USER NOT PRESENT FOR SPECIFIC PRODUCT!!!";
	public static final String ORDER_PLACED = "ORDER PLACED FROM THE CART LIST!!!";
	public static final String FOR_CORPORATE_MIN_5 = "FOR CORPORATE MINIMUM QUANTITY SHOULD BE 5";
	public static final String FOR_PERSONAL_MAX_3 = "FOR PERSONAL MAXIMUM QUANTITY SHOULD BE 3";
	public static final String QUANTITY_LESS = "OUT OF STOCK!!!";
	public static final String USER_DTO_SIZE_ERROR = "USERDTO SIZE SHOULD NOT BE MORE THAN THE QUANTITY PURCHASING";
	public static final String PRODUCT_NOT_AVAILABLE = "PRODUCT NOT AVAILABLE";
	public static final String CUSTOMER_NOT_REGISTERED = "CUSTOMER IS NOT PRESENT!!!";
	public static final String CATEGORY_ERROR = "CATEGORY IS NOT ALLOWED!!!";
	public static final String PENDING = "PENDING";
	public static final String UPDATED_SUCCESSFULLY = "UPDATED SUCCESSFULLY IN THE CART!!!";
	public static final String PERSONAL = "PERSONAL";
	public static final String CORPORATE = "CORPORATE";
	public static final String ADDED = "PRODUCT ADDED SUCCESSFULLY IN THE CART!!!";
	public static final String MAXIMUM_VALUE="GIVE MAXIMUM QUANTITY REQUIRED FOR CORPORATE CATEGORY"; 	
	public static final String MINIMUM_VALUE="GIVE MINIMUM QUANTITY REQUIRED FOR PERSONEL CATEGORY"; 

}