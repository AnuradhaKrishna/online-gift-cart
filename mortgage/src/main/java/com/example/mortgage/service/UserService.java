package com.example.mortgage.service;

import org.springframework.stereotype.Service;

import com.example.mortgage.dto.LoginRequestDto;
import com.example.mortgage.dto.LoginResponseDto;
import com.example.mortgage.dto.SignUpRequestDto;
import com.example.mortgage.dto.SignUpResponseDto;
import com.example.mortgage.exception.MortgageException;

@Service
public interface UserService {
	
	public SignUpResponseDto signUp(SignUpRequestDto signUpRequestDto) throws MortgageException;
	 public LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws MortgageException;

}
