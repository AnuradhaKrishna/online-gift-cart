package com.example.mortgage.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.mortgage.dto.EmiRequestDto;
import com.example.mortgage.dto.EmiResponseDto;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;

@Service
public interface AccountService {
	public EmiResponseDto emiCalculation(EmiRequestDto emiRequestDto) throws MortgageException;

	List<Transaction> history(Long accountNumber) throws MortgageException;

}
