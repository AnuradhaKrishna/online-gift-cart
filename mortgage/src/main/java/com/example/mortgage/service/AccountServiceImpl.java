package com.example.mortgage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mortgage.constants.ApplicationConstants;
import com.example.mortgage.dto.EmiRequestDto;
import com.example.mortgage.dto.EmiResponseDto;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.repository.TransactionRepository;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	TransactionRepository transactionRepository;

	/**
     * This method is used to calculate the emi based on the given fields
     *
     * @author Haran
     * @since 2020-03-31
     * @body We are sending the propertyCost,initialDeposit,rateOfInterest and tenure to calculate emiAmount
     * @return We are getting the Emiamount based on the given fields along with
     *         status code and message
     * @throws MortgageException
     */
	@Override
	public EmiResponseDto emiCalculation(EmiRequestDto emiRequestDto) throws MortgageException {
		EmiResponseDto emiResponseDto = new EmiResponseDto();
		Double propertyCost = emiRequestDto.getPropertyCost();
		Double initialDeposit = emiRequestDto.getInitialDeposit();
		Double rateOfInterest = emiRequestDto.getRateOfInterest();
		Integer tenure = emiRequestDto.getTenure();
		Double fixedInitialCost=(propertyCost*0.2); 
		log.info("Property Cost" +fixedInitialCost); 
		if (initialDeposit < fixedInitialCost ) {
			throw new MortgageException(ApplicationConstants.PROPER_INITIAL_AMOUNT);
		}
		if (tenure < 12) {
			throw new MortgageException(ApplicationConstants.TENSURE_VALID);
		}
		Double totalInterest = ((propertyCost - initialDeposit) * (rateOfInterest / 100));
		Double perDayInterest = (totalInterest * 30) / 365;
		Double actualAmount = propertyCost - initialDeposit;
		Double zeroInterest = actualAmount / tenure;
		Double emiAmount = perDayInterest + zeroInterest;
		emiResponseDto.setMessage(ApplicationConstants.EMI_CALC + emiAmount);
		emiResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		emiResponseDto.setEmiAmount(emiAmount);
		return emiResponseDto;
	}

	
    /**
     * @author Bairavi
     *
     *         Method is used to get the Transaction History based on the accountNumber
     *
     * @param  PathVariable is accountNumber
     * @return List<Transaction> that includes AccountNumber, EmiAmount, TransactionDate, TransactionId, and
     *         TransactionType
     *
     * @throws MortgageException
     */
 

	@Override
	public List<Transaction> history(Long accountNumber) throws MortgageException {
		Optional<List<Transaction>> transactionList = transactionRepository.findByAccountNumber(accountNumber);
		List<Transaction> transactionsList = new ArrayList<>();
		if (!transactionList.isPresent()) {
			throw new MortgageException(ApplicationConstants.ACCOUNT_NOT_REGISTERED);
		}
		transactionList.get().forEach(transaction -> {
			Transaction transactions = new Transaction();
			transactions.setAccountNumber(transaction.getAccountNumber());
			transactions.setEmiAmount(transaction.getEmiAmount());
			transactions.setTransactionDate(transaction.getTransactionDate());
			transactions.setTransactionId(transaction.getTransactionId());
			transactions.setTransactionType(transaction.getTransactionType());
			transactionsList.add(transactions);
		});
		return transactionsList;
	}
}
