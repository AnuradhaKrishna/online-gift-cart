package com.example.mortgage.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer customerId;	
	@Column(name="PASSWORD" , unique = true)
	private String password;
    @Column(name="LOGINID" , unique = true)
	private String loginId;
	private String customerName;
	private LocalDate dob;
	@Column(name="EMAILID" , unique = true)
	private String emailId;
	private Double salary;
	@Column(name="PAN" , unique = true)
	private String panNumber;
	private String occupation;
	private String employementStatus;


}
