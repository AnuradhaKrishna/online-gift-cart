package com.example.mortgage.exception;

public class MortgageException extends Exception {
	private static final long serialVersionUID = 1L;

	public MortgageException(String exception) {

		super(exception);
	}

}
