package com.example.mortgage.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mortgage.entity.Account;
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

	Optional<List<Account>> findByCustomerId(Integer customerId);

	Account findByCustomerIdAndAccountType(Integer customerId, String accountType);

}
