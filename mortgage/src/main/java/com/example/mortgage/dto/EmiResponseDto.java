package com.example.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmiResponseDto {
	
	private Double emiAmount;
	private String message;
	private Integer statusCode;

}
