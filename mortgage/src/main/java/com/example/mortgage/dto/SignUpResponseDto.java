package com.example.mortgage.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignUpResponseDto {
	
	private Integer statusCode;
	private String message;
	private CustomerResponseDto customerDto;

}
