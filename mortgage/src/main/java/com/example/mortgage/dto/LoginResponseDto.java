package com.example.mortgage.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponseDto {

	private String message;
	private Integer statusCode;
	private String customerName;
	private Integer customerId;
	private List<AccountDto> accountDto;

}
