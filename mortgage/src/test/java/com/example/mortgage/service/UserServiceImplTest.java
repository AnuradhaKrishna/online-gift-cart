package com.example.mortgage.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.mortgage.dto.CustomerResponseDto;
import com.example.mortgage.dto.LoginRequestDto;
import com.example.mortgage.dto.LoginResponseDto;
import com.example.mortgage.dto.SignUpRequestDto;
import com.example.mortgage.dto.SignUpResponseDto;
import com.example.mortgage.entity.Account;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.entity.Mortgage;
import com.example.mortgage.entity.Transaction;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.repository.AccountRepository;
import com.example.mortgage.repository.CustomerRepository;
import com.example.mortgage.repository.MortgageRepository;
import com.example.mortgage.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	AccountRepository accountRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	TransactionRepository transactionRepository;

	@Mock
	MortgageRepository mortgageRepository;

	Transaction transaction;
	List<Transaction> transactionList;
	Transaction transaction2;
	Account account;
	Account account2;
	List<Account> accountList;
	SignUpRequestDto signUpRequestDto = null;
	SignUpResponseDto signUpResponseDto = null;
	Mortgage mortgage = null;
	Customer customer = null;
	CustomerResponseDto customerResponseDto = null;

	@org.junit.Before
	public void setUp() {
		transaction = new Transaction();
		transaction.setAccountNumber(12345L);
		transaction.setEmiAmount(1000d);
		transaction.setTransactionId(1);
		transaction.setTransactionType("debit");
		transaction.setTransactionDate(LocalDate.now());

		transaction2 = new Transaction();
		transaction2.setAccountNumber(34556L);
		transaction2.setEmiAmount(1000d);
		transaction2.setTransactionId(1);
		transaction2.setTransactionType("debit");
		transaction2.setTransactionDate(LocalDate.now());

		transactionList = new ArrayList<>();
		transactionList.add(transaction2);

		account = new Account();
		account.setAccountId(1);
		account.setAccountNumber(transaction.getAccountNumber());
		account.setAccountType("mortgage");
		account.setBalance(1000d);
		account.setCreatedDate(LocalDate.now());
		account.setCustomerId(1);

		account2 = new Account();
		account2.setAccountId(1);
		account2.setAccountNumber(transaction.getAccountNumber());
		account2.setAccountType("mortgage");
		account2.setBalance(1000d);
		account2.setCreatedDate(LocalDate.now());
		account2.setCustomerId(1);

		accountList = new ArrayList<>();
		accountList.add(account);
		accountList.add(account2);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(2000D);
		mortgage.setInitialDeposit(45000D);

	}

	@Test
	public void batchProcess() {

		Mockito.when(accountRepository.findByCustomerIdAndAccountType(Mockito.anyInt(), Mockito.any()))
				.thenReturn(account);

		Mockito.when(accountRepository.findAll()).thenReturn(accountList);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Mockito.when(accountRepository.save(account2)).thenReturn(account2);

		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		Mockito.when(transactionRepository.save(transaction2)).thenReturn(transaction2);

		Mockito.when(mortgageRepository.findByCustomerId(Mockito.anyInt())).thenReturn(mortgage);

		String actual = userServiceImpl.batchProcess();
		assertNotNull(actual);

	}

	@Test
	public void batchProcess2() {

		account2.setBalance(-1000d);
		account.setBalance(-1000d);

		Mockito.when(accountRepository.findByCustomerIdAndAccountType(Mockito.anyInt(), Mockito.any()))
				.thenReturn(account);

		Mockito.when(accountRepository.findAll()).thenReturn(accountList);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		Mockito.when(accountRepository.save(account2)).thenReturn(account2);

		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		Mockito.when(transactionRepository.save(transaction2)).thenReturn(transaction2);
		Mockito.when(mortgageRepository.findByCustomerId(Mockito.anyInt())).thenReturn(mortgage);

		String actual = userServiceImpl.batchProcess();
		assertNotNull(actual);
	}

	@Test
	public void testSignup() throws Exception {
		signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setInitialDeposit(40000D);
		signUpRequestDto.setDob(LocalDate.of(1991, 06, 18));
		signUpRequestDto.setEmailId("raja@gmail.com");
		signUpRequestDto.setEmployementStatus("employee");
		signUpRequestDto.setOccupation("professional");
		signUpRequestDto.setPropertyCost(150000D);
		signUpRequestDto.setSalary(3000D);
		signUpRequestDto.setEmiAmount(2000D);
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setTenure(24);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(2000D);
		mortgage.setInitialDeposit(40000D);
		mortgage.setMortgageId(1);
		mortgage.setOperationType("REGULAR");
		mortgage.setPropertyCost(150000D);
		mortgage.setRateOfInterest(5D);
		mortgage.setTenure(24);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setLoginId("Lakshmi123");
		customer.setPassword("Lakshmi@123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setLoginId("12345");
		customerResponseDto.setPassword("23345");

		signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setStatusCode(201);
		signUpResponseDto.setCustomerDto(customerResponseDto);
		signUpResponseDto.setMessage("Success!!!");

		account = new Account();
		account.setAccountNumber(12345L);
		account.setCustomerId(1);

		transaction = new Transaction();
		transaction.setTransactionId(1);

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		SignUpResponseDto actual = userServiceImpl.signUp(signUpRequestDto);
		assertNotNull(actual);
	}

	@Test(expected = MortgageException.class)
	public void testSignup1() throws MortgageException {
		signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setInitialDeposit(40000D);
		signUpRequestDto.setDob(LocalDate.of(1991, 06, 18));
		signUpRequestDto.setEmailId("raja@gmail.com");
		signUpRequestDto.setEmployementStatus("employee");
		signUpRequestDto.setOccupation("professional");
		signUpRequestDto.setPropertyCost(1500000D);
		signUpRequestDto.setSalary(3000D);
		signUpRequestDto.setEmiAmount(2000D);
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setTenure(24);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(2000D);
		mortgage.setInitialDeposit(40000D);
		mortgage.setMortgageId(1);
		mortgage.setOperationType("REGULAR");
		mortgage.setPropertyCost(1500000D);
		mortgage.setRateOfInterest(5D);
		mortgage.setTenure(24);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setLoginId("Lakshmi123");
		customer.setPassword("Lakshmi@123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setLoginId("12345");
		customerResponseDto.setPassword("23345");

		signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setStatusCode(201);
		signUpResponseDto.setCustomerDto(customerResponseDto);
		signUpResponseDto.setMessage("Success!!!");

		account = new Account();
		account.setAccountNumber(12345L);
		account.setCustomerId(1);

		transaction = new Transaction();
		transaction.setTransactionId(1);

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		userServiceImpl.signUp(signUpRequestDto);
	}

	@Test(expected = MortgageException.class)
	public void testSignup6() throws MortgageException {
		signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setInitialDeposit(40000D);
		signUpRequestDto.setDob(LocalDate.of(1991, 06, 18));
		signUpRequestDto.setEmailId("raja@gmail.com");
		signUpRequestDto.setEmployementStatus("employee");
		signUpRequestDto.setOccupation("professional");
		signUpRequestDto.setPropertyCost(50000D);
		signUpRequestDto.setSalary(3000D);
		signUpRequestDto.setEmiAmount(2000D);
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setTenure(24);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(2000D);
		mortgage.setInitialDeposit(40000D);
		mortgage.setMortgageId(1);
		mortgage.setOperationType("REGULAR");
		mortgage.setPropertyCost(50000D);
		mortgage.setRateOfInterest(5D);
		mortgage.setTenure(24);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setLoginId("Lakshmi123");
		customer.setPassword("Lakshmi@123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setLoginId("12345");
		customerResponseDto.setPassword("23345");

		signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setStatusCode(201);
		signUpResponseDto.setCustomerDto(customerResponseDto);
		signUpResponseDto.setMessage("Success!!!");

		account = new Account();
		account.setAccountNumber(12345L);
		account.setCustomerId(1);

		transaction = new Transaction();
		transaction.setTransactionId(1);

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		userServiceImpl.signUp(signUpRequestDto);
	}

	/*
	 * @Test(expected = MortgageException.class) public void testSignup4() throws
	 * MortgageException { signUpRequestDto = new SignUpRequestDto();
	 * signUpRequestDto.setInitialDeposit(40000D);
	 * signUpRequestDto.setDob(LocalDate.of(1991, 06, 18));
	 * signUpRequestDto.setEmailId("raja@gmail.com");
	 * signUpRequestDto.setEmployementStatus("employee");
	 * signUpRequestDto.setOccupation("professional");
	 * signUpRequestDto.setPropertyCost(1500000D);
	 * signUpRequestDto.setSalary(3000D); signUpRequestDto.setEmiAmount(2000D);
	 * signUpRequestDto.setOperationType("REGULAR");
	 * signUpRequestDto.setPanNumber("EMPLYGDJ789");
	 * signUpRequestDto.setRateOfInterest(5D); signUpRequestDto.setTenure(24);
	 * 
	 * mortgage = new Mortgage(); mortgage.setCustomerId(1);
	 * mortgage.setEmiAmount(2000D); mortgage.setInitialDeposit(40000D);
	 * mortgage.setMortgageId(1); mortgage.setOperationType("REGULAR");
	 * mortgage.setPropertyCost(1500000D); mortgage.setRateOfInterest(5D);
	 * mortgage.setTenure(24);
	 * 
	 * customer = new Customer(); customer.setCustomerId(1);
	 * customer.setLoginId("Lakshmi123"); customer.setPassword("Lakshmi@123");
	 * 
	 * customerResponseDto = new CustomerResponseDto();
	 * customerResponseDto.setLoginId("12345");
	 * customerResponseDto.setPassword("23345");
	 * 
	 * signUpResponseDto = new SignUpResponseDto();
	 * signUpResponseDto.setStatusCode(201);
	 * signUpResponseDto.setCustomerDto(customerResponseDto);
	 * signUpResponseDto.setMessage("Success!!!");
	 * 
	 * account = new Account(); account.setAccountNumber(12345L);
	 * account.setCustomerId(1);
	 * 
	 * transaction = new Transaction(); transaction.setTransactionId(1);
	 * 
	 * Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
	 * Mockito.when(accountRepository.save(account)).thenReturn(account);
	 * userServiceImpl.signUp(signUpRequestDto); }
	 */

	@Test(expected = MortgageException.class)
	public void testSignup5() throws MortgageException {
		signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setInitialDeposit(40000D);
		signUpRequestDto.setDob(LocalDate.of(2008, 06, 18));
		signUpRequestDto.setEmailId("raja@gmail.com");
		signUpRequestDto.setEmployementStatus("employee");
		signUpRequestDto.setOccupation("professional");
		signUpRequestDto.setPropertyCost(150000D);
		signUpRequestDto.setSalary(3000D);
		signUpRequestDto.setEmiAmount(2000D);
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setTenure(24);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(2000D);
		mortgage.setInitialDeposit(40000D);
		mortgage.setMortgageId(1);
		mortgage.setOperationType("REGULAR");
		mortgage.setPropertyCost(150000D);
		mortgage.setRateOfInterest(5D);
		mortgage.setTenure(24);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setLoginId("Lakshmi123");
		customer.setPassword("Lakshmi@123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setLoginId("12345");
		customerResponseDto.setPassword("23345");

		signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setStatusCode(201);
		signUpResponseDto.setCustomerDto(customerResponseDto);
		signUpResponseDto.setMessage("Success!!!");

		account = new Account();
		account.setAccountNumber(12345L);
		account.setCustomerId(1);

		transaction = new Transaction();
		transaction.setTransactionId(1);

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		userServiceImpl.signUp(signUpRequestDto);
	}

	@Test(expected = MortgageException.class)
	public void testSignup2() throws MortgageException {
		signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setInitialDeposit(40000D);
		signUpRequestDto.setDob(LocalDate.of(1991, 06, 18));
		signUpRequestDto.setEmailId("raja@gmail.com");
		signUpRequestDto.setEmployementStatus("employee");
		signUpRequestDto.setOccupation("professional");
		signUpRequestDto.setPropertyCost(150000D);
		signUpRequestDto.setSalary(30000D);
		signUpRequestDto.setEmiAmount(27000D);
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setTenure(24);

		mortgage = new Mortgage();
		mortgage.setCustomerId(1);
		mortgage.setEmiAmount(27000D);
		mortgage.setInitialDeposit(40000D);
		mortgage.setMortgageId(1);
		mortgage.setOperationType("REGULAR");
		mortgage.setPropertyCost(150000D);
		mortgage.setRateOfInterest(5D);
		mortgage.setTenure(24);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setLoginId("Lakshmi123");
		customer.setPassword("Lakshmi@123");

		customerResponseDto = new CustomerResponseDto();
		customerResponseDto.setLoginId("12345");
		customerResponseDto.setPassword("23345");

		signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setStatusCode(201);
		signUpResponseDto.setCustomerDto(customerResponseDto);
		signUpResponseDto.setMessage("Success!!!");

		account = new Account();
		account.setAccountNumber(12345L);
		account.setCustomerId(1);

		transaction = new Transaction();
		transaction.setTransactionId(1);

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		userServiceImpl.signUp(signUpRequestDto);
	}

	@Test
	public void userLoginTest() throws MortgageException {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setLoginId("123");
		loginRequestDto.setPassword("098");

		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setCustomerId(1);
		loginResponseDto.setCustomerName("Baiu");
		loginResponseDto.setMessage("Success");
		loginResponseDto.setStatusCode(200);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");

		Account account = new Account();
		account.setAccountNumber(1234667l);
		account.setAccountType("Saving");
		account.setBalance(12000D);
		List<Account> acc = new ArrayList<>();
		acc.add(account);

		Mockito.when(customerRepository.findByLoginIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByCustomerId(Mockito.anyInt())).thenReturn(Optional.of(acc));
		LoginResponseDto result = userServiceImpl.userLogin(loginRequestDto);
		assertNotNull(result);

	}

	@Test(expected = MortgageException.class)
	public void userLoginTestForNegative() throws MortgageException {
		LoginRequestDto loginRequestDto = new LoginRequestDto();

		Mockito.when(customerRepository.findByLoginIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.ofNullable(null));
		userServiceImpl.userLogin(loginRequestDto);
	}

}
