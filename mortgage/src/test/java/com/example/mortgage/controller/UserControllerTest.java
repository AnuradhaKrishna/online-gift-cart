package com.example.mortgage.controller;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.example.mortgage.dto.CustomerResponseDto;
import com.example.mortgage.dto.LoginRequestDto;
import com.example.mortgage.dto.LoginResponseDto;
import com.example.mortgage.dto.SignUpRequestDto;
import com.example.mortgage.dto.SignUpResponseDto;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.exception.MortgageException;
import com.example.mortgage.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserService userService;

	@Test
	public void userLoginTest() throws MortgageException {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setLoginId("123");
		loginRequestDto.setPassword("098");
		
		Customer customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setLoginId("123");
		customer.setPassword("098");
		
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setCustomerId(1);
		loginResponseDto.setCustomerName("Baiu");
		loginResponseDto.setMessage("Success");
		loginResponseDto.setStatusCode(200);

		Mockito.when(userService.userLogin(Mockito.any())).thenReturn(loginResponseDto);
		ResponseEntity<LoginResponseDto> result = userController.userLogin(loginRequestDto);
		assertNotNull(result);
	}
	
	@Test
	public void signUpTest() throws MortgageException {
		SignUpRequestDto signUpRequestDto = new SignUpRequestDto();
		signUpRequestDto.setCustomerName("Anuradha");
		signUpRequestDto.setDob(LocalDate.now());
		signUpRequestDto.setEmailId("anuradha@gmail.com");
		signUpRequestDto.setEmiAmount(2000D);
		signUpRequestDto.setEmployementStatus("SELF");
		signUpRequestDto.setInitialDeposit(20000D);
		signUpRequestDto.setOccupation("JOB");
		signUpRequestDto.setOperationType("REGULAR");
		signUpRequestDto.setPanNumber("EMPLYGDJ789");
		signUpRequestDto.setPropertyCost(2000000D);
		signUpRequestDto.setRateOfInterest(5D);
		signUpRequestDto.setSalary(29000D);
		signUpRequestDto.setTenure(24);
		
		CustomerResponseDto customerDto = new CustomerResponseDto();
		customerDto.setLoginId("Anuradha123");
		customerDto.setPassword("Anuradha@07");
		
		SignUpResponseDto signUpResponseDto = new SignUpResponseDto();
		signUpResponseDto.setMessage("GRANTED!!!");
		signUpResponseDto.setStatusCode(607);
		signUpResponseDto.setCustomerDto(customerDto);
		
		Mockito.when(userService.signUp(Mockito.any())).thenReturn(signUpResponseDto);
		ResponseEntity<SignUpResponseDto> result = userController.signUp(signUpRequestDto);
		assertNotNull(result);
	}
}
